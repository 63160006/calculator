import "dart:io";
import 'dart:math';

void main(List<String> arguments) {
  print("input your number");
  String input = (stdin.readLineSync()!);
  List input1 = tokenString(input);
  List postfix = inToPost(input1);
  stdout.write("result is : ${EvaluatePostfix(postfix)}");
}

List tokenString(String input) {
  List token = [];
  String empty = "";
  for (int i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (!empty.isEmpty) {
        token.add(empty);
        empty = "";
      } else {}
    } else {
      empty += input[i];
    }
  }
  if (input[input.length - 1].contains("")) {
    token.add(empty);
  }
  return token;
}

bool OpSign(String x) {
  return x == "+" || x == "-" || x == "*" || x == "/" || x == "^";
}

int sort(String x) {
  return (x == "+" || x == "-")
      ? 1
      : (x == "*" || x == "/")
          ? 2
          : (x == "^")
              ? 3
              : 0;
}

bool InfixSyntax(String x) {
  return x == "+" ||
      x == "-" ||
      x == "*" ||
      x == "/" ||
      x == "^" ||
      x == "(" ||
      x == ")" ||
      int.tryParse(x) != null;
}

List inToPost(List inExpres) {
  var OP = [];
  var postfix = [];

  for (int i = 0; i < inExpres.length; i++) {
    var token = inExpres[i];
    if (!InfixSyntax(token)) continue;
    if (int.tryParse(token) != null) {
      postfix.add(int.tryParse(token));
    }
    if (OpSign(token)) {
      while (OP.isNotEmpty && OP.last != "(" && sort(token) < sort(OP.last)) {
        postfix.add(OP.removeLast());
      }
      OP.add(token);
    }
    if (token == "(") {
      OP.add(token);
    }
    if (token == ")") {
      while (OP.last != "(") {
        postfix.add(OP.removeLast());
      }
      OP.removeLast();
    }
  }
  while (OP.isNotEmpty) {
    postfix.add(OP.removeLast());
  }
  return postfix;
}

num EvaluatePostfix(List input) {
  var values = [];
  var right;
  var left;
  var sum;
  for (int i = 0; i < input.length; i++) {
    if (input[i] is int) {
      values.add(input[i]);
    } else {
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      if (input[i] == '+') {
        sum = left + right;
      } else if (input[i] == '-') {
        sum = left - right;
      } else if (input[i] == '*') {
        sum = left * right;
      } else if (input[i] == '/') {
        sum = left / right;
      } else if (input[i] == '^') {
        sum = pow(left, right);
      } else if (input[i] == '%' ){
        sum = left % right;
      }
      values.add(sum);
    }
  }
  return values.first;
}